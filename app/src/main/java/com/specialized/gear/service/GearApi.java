package com.specialized.gear.service;

import com.specialized.gear.model.Gear;
import com.specialized.gear.model.GearDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GearApi {

    @GET("gear/")
    Call<List<Gear>> getGears();

    @GET("gear/{id}/")
    Call<GearDetails> getGearDetails(@Path("id") int id);

}
