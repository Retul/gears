package com.specialized.gear.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.specialized.gear.R;
import com.specialized.gear.model.SubGear;
import com.specialized.gear.view.SubGearViewHolder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SubGearAdapter extends RecyclerView.Adapter<SubGearViewHolder> {
    List<SubGear> subGearList = new ArrayList<>();
    Context context;

    public SubGearAdapter(Context c) {
        context = c;
    }

    @NonNull
    @Override
    public SubGearViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.subgear_item, viewGroup, false);
        SubGearViewHolder vh = new SubGearViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SubGearViewHolder subGearViewHolder, int i) {
        subGearViewHolder.setData(subGearList.get(i));
    }

    @Override
    public int getItemCount() {
        return subGearList.size();
    }

    public void updateList(List<SubGear> s) {
        subGearList.addAll(s);
        subGearList.sort(new Comparator<SubGear>() {
            @Override
            public int compare(SubGear o1, SubGear o2) {
                if (o1.getOrder().equals(o2.getOrder())) {
                    return 0;
                } else {
                    return o1.getOrder() < o2.getOrder() ? -1 : 1;
                }
            }
        });
        notifyDataSetChanged();
    }
}
