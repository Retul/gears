package com.specialized.gear.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.specialized.gear.R;
import com.specialized.gear.model.GearDetails;
import com.specialized.gear.view.GearViewHolder;
import com.specialized.gear.view.MainActivity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GearAdapter extends RecyclerView.Adapter<GearViewHolder> {
    private List<GearDetails> gearDetailList = new ArrayList<>();
    private Context context;
    private MainActivity.GearClickListener listener;


    public GearAdapter(Context c, MainActivity.GearClickListener l) {
        context = c;
        listener = l;

    }

    @NonNull
    @Override
    public GearViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.gear_item, viewGroup, false);
        GearViewHolder vh = new GearViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final GearViewHolder feedViewHolder, int i) {
        feedViewHolder.setData(gearDetailList.get(i), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGearClicked(gearDetailList.get(feedViewHolder.getAdapterPosition()));

            }
        });

    }

    @Override
    public int getItemCount() {
        return gearDetailList.size();
    }

    public void updateList(List<GearDetails> g) {
        gearDetailList.addAll(g);
        gearDetailList.sort(new Comparator<GearDetails>() {
            @Override
            public int compare(GearDetails o1, GearDetails o2) {
                if (o1.getOrder().equals(o2.getOrder())) {
                    return 0;
                } else {
                    return o1.getOrder() < o2.getOrder() ? -1 : 1;
                }
            }
        });
        notifyDataSetChanged();
    }
}
