package com.specialized.gear.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.specialized.gear.R;
import com.specialized.gear.model.GearDetails;
import com.squareup.picasso.Picasso;

public class GearViewHolder extends RecyclerView.ViewHolder {

    private TextView gearNameTextView;
    private TextView descriptionNameTextView;
    private ImageView gearImage;
    private View root;

    public GearViewHolder(@NonNull View itemView) {
        super(itemView);
        gearNameTextView = itemView.findViewById(R.id.gear_name);
        descriptionNameTextView = itemView.findViewById(R.id.gear_description);
        gearImage = itemView.findViewById(R.id.gear_image);
        root = itemView;
    }

    public void setData(GearDetails gearDetails, View.OnClickListener listener) {
        gearNameTextView.setText(gearDetails.getName());
        descriptionNameTextView.setText(gearDetails.getDescription());
        Picasso.get().load(gearDetails.getImage()).fit().centerCrop().into(gearImage);
        root.setOnClickListener(listener);
    }
}
