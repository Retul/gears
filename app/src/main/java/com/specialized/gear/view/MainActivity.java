package com.specialized.gear.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.specialized.gear.R;
import com.specialized.gear.controller.GearAdapter;
import com.specialized.gear.model.Gear;
import com.specialized.gear.model.GearDetails;
import com.specialized.gear.service.GearApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String BASE_URL = "https://interview.retul.com/api/";

    private GearAdapter adapter;
    private Integer totalResponseCount = 0;
    private List<GearDetails> accumulator = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new GearAdapter(this, new GearClickListener() {
            @Override
            public void onGearClicked(GearDetails gearDetails) {
                startGearDetailsActivity(gearDetails);
            }
        });

        recyclerView.setAdapter(adapter);

        getGearDetails();
    }

    private void getGearDetails() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        final GearApi gearApi = retrofit.create(GearApi.class);

        Call<List<Gear>> getGearCall = gearApi.getGears();
        getGearCall.enqueue(new Callback<List<Gear>>() {
            @Override
            public void onResponse(Call<List<Gear>> call, Response<List<Gear>> response) {
                final List<Gear> gears = response.body();

                for (int i = 0; i < gears.size(); i++) {
                    Call<GearDetails> getGearDetailsCall = gearApi.getGearDetails(gears.get(i).getId());

                    getGearDetailsCall.enqueue(new Callback<GearDetails>() {
                        @Override
                        public void onResponse(Call<GearDetails> call, Response<GearDetails> response) {
                            totalResponseCount++;
                            accumulator.add(response.body());
                            if (totalResponseCount == gears.size()) {
                                adapter.updateList(accumulator);
                            }
                        }

                        @Override
                        public void onFailure(Call<GearDetails> call, Throwable t) {
                            Log.d("On failure: ", t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Gear>> call, Throwable t) {
                Log.d("On failure: ", t.getMessage());
            }
        });
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.abs_layout);
        }
    }

    public interface GearClickListener {
        void onGearClicked(GearDetails gearDetails);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public void startGearDetailsActivity(GearDetails gearDetails) {
        Intent intent = new Intent(MainActivity.this, GearDetailsActivity.class);
        intent.putExtra("gearDetails", gearDetails);
        startActivity(intent);
    }
}