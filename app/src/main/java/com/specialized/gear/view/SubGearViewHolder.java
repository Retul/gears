package com.specialized.gear.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.specialized.gear.R;
import com.specialized.gear.model.SubGear;
import com.squareup.picasso.Picasso;

public class SubGearViewHolder extends RecyclerView.ViewHolder {

    private TextView gearNameTextView;
    private TextView descriptionNameTextView;
    private ImageView gearImage;

    public SubGearViewHolder(@NonNull View itemView) {
        super(itemView);
        gearNameTextView = itemView.findViewById(R.id.subgear_name);
        descriptionNameTextView = itemView.findViewById(R.id.gear_detail_description);
        gearImage = itemView.findViewById(R.id.subgear_image);

    }

    public void setData(SubGear subGear) {
        gearNameTextView.setText(subGear.getName());
        descriptionNameTextView.setText(subGear.getDescription());
        Picasso.get().load(subGear.getImage()).fit().centerCrop().into(gearImage);

    }
}
