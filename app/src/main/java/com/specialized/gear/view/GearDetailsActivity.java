package com.specialized.gear.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.specialized.gear.R;
import com.specialized.gear.controller.SubGearAdapter;
import com.specialized.gear.model.GearDetails;
import com.specialized.gear.model.SubGear;
import com.squareup.picasso.Picasso;

import java.util.function.BinaryOperator;
import java.util.function.Function;

public class GearDetailsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gear_detail_layout);

        GearDetails gearDetails = getIntent().getParcelableExtra("gearDetails");

        setupToolbar();

        ImageView gearImageDetail = findViewById(R.id.gear_detail_image_view);
        TextView gearName = findViewById(R.id.gear_name_detail_text);
        TextView gearPrice = findViewById(R.id.gear_price_text);
        TextView gearDesc = findViewById(R.id.gear_detail_description);

        SubGearAdapter adapter = new SubGearAdapter(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = findViewById(R.id.sub_gear_recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Picasso.get().load(gearDetails.getImage()).resize(1000, 1000).centerInside().into(gearImageDetail);
        gearName.setText(gearDetails.getName());
        gearDesc.setText(gearDetails.getDescription());
        adapter.updateList(gearDetails.getSubGears());


        Double total = getTotal(gearDetails);
        if (total == 0) {
            gearPrice.setVisibility(View.GONE);
        } else {
            gearPrice.setVisibility(View.VISIBLE);
            gearPrice.setText(String.format("%s%s%s", getApplicationContext().getResources().getString(R.string.price), total.toString(), getApplicationContext().getResources().getString(R.string.zero)));
        }
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.abs_details_layout);
        }
    }

    public Double getTotal(GearDetails gearDetails) {
        Double total = 0.00;
        if (gearDetails.getSubGears() != null && gearDetails.getSubGears().size() > 0) {
            total = gearDetails.getSubGears().stream().map(new Function<SubGear, Double>() {
                @Override
                public Double apply(SubGear subGear) {
                    if (subGear.getPrice() != null) {
                        return Double.parseDouble(String.valueOf(Math.round(Float.parseFloat(subGear.getPrice()))));
                    }
                    return 0.0;
                }
            }).reduce(new BinaryOperator<Double>() {
                @Override
                public Double apply(Double a, Double a2) {
                    return a + a2;
                }
            }).get();
        }
        return total;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
}