
package com.specialized.gear.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GearDetails implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("sub_gears")
    @Expose
    private List<SubGear> subGears = null;

    public GearDetails() {

    }
    protected GearDetails(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        image = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            order = null;
        } else {
            order = in.readInt();
        }
        description = in.readString();
        subGears = in.createTypedArrayList(SubGear.CREATOR);



    }

    public static final Creator<GearDetails> CREATOR = new Creator<GearDetails>() {
        @Override
        public GearDetails createFromParcel(Parcel in) {
            return new GearDetails(in);
        }

        @Override
        public GearDetails[] newArray(int size) {
            return new GearDetails[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SubGear> getSubGears() {
        return subGears;
    }

    public void setSubGears(List<SubGear> subGears) {
        this.subGears = subGears;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(image);
        dest.writeString(name);
        if (order == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(order);
        }
        dest.writeString(description);
        dest.writeTypedList(subGears);

    }
}
